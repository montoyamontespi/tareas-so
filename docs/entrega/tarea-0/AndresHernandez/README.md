# Andrés Hernández

- Número de cuenta: `123456789`

Hola, esta es mi carpeta para la [*tarea-0*](https://redes-ciencias-unam.gitlab.io/2022-1/tareas-so/entrega/tarea-0).

Actividades a las que me quiero dedicar al salir de la facultad:

- Seguridad informática (red team, blue team)
- DevOps (Kubernetes)
- Desarrollo

Cosas que me gusta hacer en mi tiempo libre:

- Dormir con el gato
- Jugar videojuegos
- Ver películas
- Hacer experimentos con componentes electrónicos

| Esta es la imagen que elegí |
|:---------------------------:|
| ![](img/tonejito.png)       |
